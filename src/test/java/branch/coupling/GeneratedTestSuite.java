package coupling;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import coupling.TestClub;
import coupling.TestMember;

public class GeneratedTestSuite extends TestCase {
  public static Test suite() {
    TestSuite suite = new TestSuite();
    suite.addTestSuite(TestClub.class);
    suite.addTestSuite(TestMember.class);
    return suite;
  }
}
