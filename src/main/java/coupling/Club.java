package coupling;

import java.util.ArrayList;
import java.util.List;

/**
 * Author: ttreitlinger
 * Date  : 29/09/2011
 * Time  : 17:21
 */
public class Club {

    private String name;
    private List<Member> allMembers;
    private List<Member> vipMembers;

    public Club(String name) {
        this.name = name;
        allMembers = new ArrayList<Member>();
        vipMembers = new ArrayList<Member>();
    }

    public boolean addMember(Member member) {
        allMembers.add(member);
        return true;
    }

    public boolean makeVip(Member member){
        if (allMembers.contains(member)) {
            vipMembers.add(member);
            return true;
        }
        return false;
    }

    public double getMemberAverageHeight(){

        int count = 0;
        double total = 0;

        for (Member m : getAllMembers()) {
            count++;
            total += m.getHeight();
        }

        return total / count;
    }

    public int getVipAverageHeight(){

        int count = 0;
        int total = 0;
        for (Member m : getVipMembers()) {
            count++;
            total += m.getHeight();
        }

        return total / count;
    }

    public String toString() {
        StringBuffer result = new StringBuffer();
        result.append("Club: ").append(getName());
        result.append("Number of members: ").append(getMemberCount());
        result.append("Number of VIP members: ").append(getVipCount());
        result.append("Member average height: ").append(getMemberAverageHeight());
        return result.toString();
    }

    private int getMemberCount() {
        return allMembers.size();
    }

    private int getVipCount() {
        return vipMembers.size();
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Member> getAllMembers() {
        return allMembers;
    }

    public void setAllMembers(List<Member> allMembers) {
        this.allMembers = allMembers;
    }

    public List<Member> getVipMembers() {
        return vipMembers;
    }

    public void setVipMembers(List<Member> vipMembers) {
        this.vipMembers = vipMembers;
    }
}
