package coupling;

import java.util.Date;

/**
 * Author: ttreitlinger
 * Date  : 29/09/2011
 * Time  : 17:18
 */
public class Member {

    private String firstName;
    private String lastName;
    private int height;

    public Member(String firstName, String lastName, int height) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.height = height;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

}
